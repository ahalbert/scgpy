import urllib3
import re
import us
import string
import json

http = urllib3.PoolManager()
req = http.request('get', "http://icanhazip.com")
ip = req.data[:-1]
req = http.request('GET', 'http://api.ipinfodb.com/v3/ip-city/?key=APIKEY&ip=' + ip)
state = "MA"
town = "Amherst"
if req.status == 200:
    data = re.split(';', req.data)
    state = (us.states.lookup(data[5]).abbr).upper()
    town = data[6].replace(' ', '_')
weatherAddress = string.Template("http://api.wunderground.com/api/6d331eba0dc866d4/conditions/q/$state/$town.json").substitute({'state':state,'town':town})
req = http.request('GET', weatherAddress)
conditions =  json.loads('[' + req.data + ']')[0]
forecastAddress = string.Template("http://api.wunderground.com/api/APIKEY/forecast/q/$state/$town.json").substitute({'state':state,'town':town})
req = http.request('GET', forecastAddress)
forecast =  json.loads('[' + req.data + ']')[0]
output = "$weather, $future ${temperature}, $hi/$low F, $wind mph"
weatherdic ={'weather': conditions['current_observation']['weather'], 'future':forecast['forecast']['simpleforecast']['forecastday'][1]['conditions'], 'temperature':conditions['current_observation']['temperature_string'], 'hi': forecast['forecast']['simpleforecast']['forecastday'][0]['high']['fahrenheit'], \
    'low':forecast['forecast']['simpleforecast']['forecastday'][0]['low']['fahrenheit'], 'wind': forecast['forecast']['simpleforecast']['forecastday'][0]['avewind']['mph']}
with open('/Users/ahalbert/.weather', 'w') as weatherfile:
    weatherfile.write(string.Template(output).substitute(weatherdic))
