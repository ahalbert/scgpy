# The MIT License (MIT)

# Copyright (c) <year> <copyright holders>

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import os
import string
import shutil
import sys
import xmltodict 
import datetime
from Cheetah.Template import Template
import csv
import re

#usage python scgpy.py xmlfilename

def addPost(filename, refreshMode=False , date=datetime.date.today().strftime("%A, %B %d, %Y")):
    #path for sourcing dependencies
    filepath = re.split("/",filename)
    filepath = "/" + "/".join(filepath[1:-1]) + "/"
    #read initial xml
    with open(filename, 'r') as inputfile:
        xml = inputfile.read()
    substXML = Template(xml, searchList=[{"date" : date}]).__str__()
    xml = xmltodict.parse(substXML)
    with open('post_template.html', 'r') as templateFile:
        template = templateFile.read()
    post = Template(template, searchList=[xml['scg']]).__str__()
    xml['scg']['title'] = string.replace(xml['scg']['title'], ' ', '_')
    path = './www/' + xml['scg']['type']  + "/"
    #create folder if it does not exist, then write new file
    try:
        os.stat(path) 
    except OSError:
        os.makedirs(path)
    htmlPath =  path  + xml['scg']['title'] + ".html"
    with open(htmlPath, 'w') as postfile:
        postfile.write(post) 
    #Copy files
    globaldependencies = []
    with open("dependencies.csv", 'r') as globaldependenciesfile:
        csvreader = csv.reader(globaldependenciesfile)
        for row in csvreader:
            globaldependencies.append(row[0])
    try:
        if type(xml['scg']['dependency'] ) is unicode:
            xml['scg']['dependency'] = [filepath + xml['scg']['dependency']] + globaldependencies
        else:
            xml['scg']['dependency'] = [addPath(p,filepath) for p in xml['scg']['dependency']  ] 
            xml['scg']['dependency'] = xml['scg']['dependency'] + globaldependencies
    except KeyError:
            xml['scg']['dependency'] =  globaldependencies
    for item in xml['scg']['dependency']: 
        shutil.copy2(item, path)
    #metadata
    metadata = [[filename, date]]
    with open('metadata.csv', 'r') as csvfile:
        csvreader = csv.reader(csvfile)
        for row in csvreader:
            metadata.append(row)
    try:
        if metadata[0][0] != metadata[1][0] and not refreshMode:
            with open('metadata.csv', 'w') as csvfile:
                csvwriter = csv.writer(csvfile)
                for row in metadata:
                    csvwriter.writerow(row) 
    except IndexError:
       pass 
    writeArchive()

def addPath(path, filepath):
    if "/" in path:
        return path
    else:
        return filepath + path
def createSummary(post):
    post['scg']['content'] = re.split("\n", post['scg']['content']) # creates content preview
    post['scg']['content'] = post['scg']['content'][0] + post['scg']['content'][1]
    return post['scg']
#Archive page and index page    
def writeArchive():
    metadata = []
    with open('metadata.csv', 'r') as csvfile:
        csvreader = csv.reader(csvfile)
        for row in csvreader:
            metadata.append(row)
    posts = []
    for row in metadata:
        with open(row[0], 'r') as xmlfile:
            xml = xmltodict.parse(xmlfile.read())
            xml['scg']['date'] = row[1]
            xml['scg']['ftitle'] = string.replace(xml['scg']['title'], ' ', '_')
            posts.append(xml)
    summaryPosts = map(createSummary, posts[:5])
    with open('index_template.html' ,'r') as template: 
        content = Template(template.read(), searchList={"summaryPosts" : summaryPosts }).__str__()
    with open('./www/index.html' ,'w') as index: 
        index.write(content)
    with open('archive_template.html', 'r') as templateFile:
        template = templateFile.read()
        content = Template(template, searchList= {"posts":posts, "type" : "Post"}).__str__()
        rcontent = Template(template, searchList= {"posts":posts, "type" : "Reviews"}).__str__()
        tcontent = Template(template, searchList= {"posts":posts, "type" : "Tech"}).__str__()
        bcontent = Template(template, searchList= {"posts":posts, "type" : "Blog"}).__str__()
    with open('./www/archive.html', 'w') as archive:
        archive.write(content)
    with open('./www/reviews.html', 'w') as archive:
        archive.write(rcontent)
    with open('./www/tech.html', 'w') as archive:
        archive.write(tcontent)
    with open('./www/blog.html', 'w') as archive:
        archive.write(bcontent)
    #global dependencies file
    globaldependencies = []
    with open("dependencies.csv", 'r') as globaldependenciesfile:
        csvreader = csv.reader(globaldependenciesfile)
        for row in csvreader:
            shutil.copy2(row[0], "./www")
def refresh():
    with open('metadata.csv', 'r') as csvfile:
        csvreader = csv.reader(csvfile)
        for row in csvreader:
            addPost(row[0],refreshMode=True, date=row[1])

def writeProject():
    with open('projects.html', 'r') as templateFile:
        template = templateFile.read()
        content = Template(template).__str__()
    with open('./www/projects.html', 'w') as projectFile:
         projectFile.write(content)
if sys.argv[1] == "refresh":
    writeProject()
    refresh()
    writeArchive()
else:
    addPost(sys.argv[1])

